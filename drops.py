import os
import secrets

# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from time import sleep

# PATH_TO_TX_DROPPED = '/sys/class/net/{}/statistics/tx_dropped'
PATH_TO_TX_DROPPED = './tx_dropped'

INTERVAL = 1
TUNNELS = ['tun02', 'tun03', 'tun04', 'tun05']


def simulate_drops():
    dropped = [0 for _ in range(4)]
    while True:
        for i, tunnel in enumerate(TUNNELS):
            current_tx_dropped = dropped[i]
            write_to_tx_dropped_file(tunnel, current_tx_dropped)
            delta = generate_random_delta()
            dropped[i] += delta
        sleep(INTERVAL)

def write_to_tx_dropped_file(tunnel: str, current_tx_dropped: int):
    path_to_tun = PATH_TO_TX_DROPPED.format(tunnel)
    os.system(f"chmod 777 {path_to_tun} && echo {current_tx_dropped} > {path_to_tun}")


def generate_random_delta():
    return secrets.choice([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    simulate_drops()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
