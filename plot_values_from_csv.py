import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

TESTCASE = "D"
alpha = '0.125'
beta  = '0.250'
gamma = '0.350'
PATH_TO_FILE = f"/home/mattan/Documents/Work/edge-rad/Adaptive_Weights/alpha:{alpha} beta:{beta} gamma:{gamma} testcase:{TESTCASE}/tun {{}}.csv"
"../tun %u alpha:%f beta:%f gamma:%f testcase: %s.csv"

# libraries
import seaborn as sns
import pandas as pd
#
# # Make data
# data = pd.DataFrame({'group_A': [1, 4, 6, 8, 9], 'group_B': [2, 24, 7, 10, 12], 'group_C': [2, 8, 5, 10, 6], },
#                     index=range(1, 6))
#
#

def read_sample_column():
    print('{', end="")
    with open(PATH, "r") as f:
        for line in f.readlines():
            print(line.split(',',1)[0], end=",")
    print('}', end="")

if __name__ == "__main__":
    tunnel_weights = list()
    
    for i in range(4):
        path = PATH_TO_FILE.format(i)
        my_data = np.genfromtxt(path, delimiter=',')[1:]
        tunnel_weights.append(my_data[:, 3].tolist())
        plt.plot(my_data)
        dev = mpatches.Patch(color='green', label='devrtt')
        sample = mpatches.Patch(color="blue", label='sample')
        estimated = mpatches.Patch(color="orange", label='estimated')
        plt.title(f"alpha: {alpha}, beta: {beta}, gamma: {gamma}, tun # {i}")
        plt.legend(handles=[dev, sample, estimated])
        plt.show()

    # Make data
    data = pd.DataFrame({'tun_0': tunnel_weights[0], 'tun_1': tunnel_weights[1], 'tun_2': tunnel_weights[2], 'tun_3': tunnel_weights[3]})
    # We need to transform the data from raw data to percentage (fraction)
    data_perc = data.divide(data.sum(axis=1), axis=0)

    # Make the plot
    plt.stackplot(range(1, 181),
                  data_perc['tun_0'],
                  data_perc['tun_1'],
                  data_perc['tun_2'],
                  data_perc['tun_3'],
                  labels=['0', '1', '2', '3']
                  )
    plt.legend(loc='upper left')
    plt.margins(0, 0)
    plt.title('100 % stacked area chart')
    plt.show()
    # plt.imshow(dpi=1000)