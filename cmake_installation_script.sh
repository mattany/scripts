#!/bin/bash
VERSION="cmake-3.17.5-Linux-x86_64"
wget https://github.com/Kitware/CMake/releases/download/v3.17.5/$VERSION.sh
sudo chmod u+x ./$VERSION.sh
sudo ./$VERSION.sh
mv ./$VERSION /opt/
sudo ln -s /opt/$VERSION/bin/* /usr/bin*
cmake --version
 

