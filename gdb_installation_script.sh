#!/bin/bash
sudo yum groupinstall 'Development Tools'
GDB_VERSION=gdb-8.1.1
CURRENT_DIR=$PWD
wget ftp://ftp.gnu.org/gnu/gdb/$GDB_VERSION.tar.gz
tar -xvf ./$GDB_VERSION.tar.gz
cd ./$GDB_VERSION
$CURRENT_DIR/$GDB_VERSION/configure
make
ln -s /opt/gdb-8.1.1/gdb/gdb /usr/bin
ln -s /opt/gdb-8.1.1/gdb/gdbserver/gdbserver /usr/bin


