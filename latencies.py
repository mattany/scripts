import os
import secrets

# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import random
from time import sleep

# PATH_TO_TX_DROPPED = '/sys/class/net/{}/statistics/tx_dropped'
PATH_TO_TX_DROPPED = './tx_dropped'

INTERVAL = 1
TUNNELS = ['tun02', 'tun03', 'tun04', 'tun05']


def add_randomness(num: int, variance: int):
    diff = secrets.choice([i for i in range(variance + 1)])
    sign = secrets.choice([-1, 1])
    return max(0, num + sign * diff)



def generate_stable_numbers(amount: int, pivot: int, variance: int):
    return [add_randomness(pivot, variance) for _ in range(amount)]


def generate_moving_numbers(amount: int, start: int, variance: int, step: int):
    return [add_randomness(start + i * step, variance) for i in range(amount)]


def generate_testcase_A():
    """
    stable
    :return:
    """
    tun_1 = generate_stable_numbers(180, 100, 20)
    retval = add_other_three_tunnels(tun_1)
    return retval




def generate_testcase_B():
    """
    Slowly rising
    :return:
    """
    arr_1 = generate_stable_numbers(60, 100, 20)
    arr_2 = generate_moving_numbers(30, 100, 20, 5)
    arr_3 = generate_stable_numbers(30, 250, 20)
    arr_4 = generate_moving_numbers(30, 250, 20, -5)
    arr_5 = generate_stable_numbers(30, 100, 20)

    tun_1 = arr_1 + arr_2 + arr_3 + arr_4 + arr_5
    retval = add_other_three_tunnels(tun_1)
    return retval


def add_other_three_tunnels(tun_1):
    tun_2 = generate_stable_numbers(180, 50, 5)
    tun_3 = generate_stable_numbers(180, 20, 1)
    tun_4 = generate_stable_numbers(180, 10, 2)
    retval = [tun_1, tun_2, tun_3, tun_4]
    # random.shuffle(retval)
    return retval


def generate_testcase_C():
    arr_1 = generate_stable_numbers(60, 100, 20)
    arr_2 = generate_moving_numbers(3, 100, 20, 30)
    arr_3 = generate_moving_numbers(3, 100, 20, 30)
    arr_4 = generate_stable_numbers(114, 100, 20)
    tun_1 = arr_1 + arr_2 + arr_3 + arr_4
    retval = add_other_three_tunnels(tun_1)
    return retval

def generate_testcase_D():
    """
    stable, down, stable - 60 seconds each
    :return:
    """
    arr_1 = generate_stable_numbers(60, 100, 20)
    arr_2 = generate_stable_numbers(60, 400, 20)
    arr_3 = generate_stable_numbers(60, 100, 20)
    tun_1 = arr_1 + arr_2 + arr_3
    retval = add_other_three_tunnels(tun_1)
    return retval

def generate_testcase_E():
    arr_1 = generate_stable_numbers(60, 100, 20)
    arr_2 = generate_stable_numbers(3, 400, 20)
    arr_3 = generate_stable_numbers(117, 100, 20)
    tun_1 = arr_1 + arr_2 + arr_3
    retval = add_other_three_tunnels(tun_1)
    return retval

def generate_testcase_drop_to_zero():
    """
    stable, drop to zero, stable - 60 seconds each
    :return:
    """
    arr_1 = generate_stable_numbers(60, 100, 20)
    arr_2 = generate_stable_numbers(60, 0, 0)
    arr_3 = generate_stable_numbers(60, 100, 20)
    tun_1 = arr_1 + arr_2 + arr_3
    retval = add_other_three_tunnels(tun_1)
    return retval


def c_representation(testcase: list):
    return str(testcase).replace('[','{').replace(']','}')
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    for i, testcase in enumerate([generate_testcase_drop_to_zero(),]):

    # for i, testcase in enumerate((generate_testcase_A(), generate_testcase_B(), generate_testcase_C(), generate_testcase_D(), generate_testcase_E())):
        print(f"D32 testcase{chr(i + 65)}[4][{len(testcase[0])}] = {c_representation(testcase)};\n")
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
    generate_testcase_drop_to_zero()
