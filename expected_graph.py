import os
import sys
from datetime import datetime
from time import sleep

HIGH_RTT_MAX = 3
LOW_RTT_MAX = 3
highRTTCounter = 0
timeoutCounter = 0
warmUpIterations = 1000
aggregateLength = 20

deltas = [0 for _ in range(aggregateLength)]

class Parameters:
    def __init__(self):
        self.alpha = 0
        self.beta = 0
        self.gamma = 0
        self.tau = 0
        self.low_increase = 0
        self.medium_increase = 0
        self.high_increase = 0
        self.low_decrease = 0
        self.medium_decrease = 0
        self.high_decrease = 0


class Diagnostics:
    def __init__(self):
        self.sampleRTT = 0
        self.estimatedRTT = 0
        self.devRTT = 0
        self.live_quantum = 0

params = Parameters()
diag = Diagnostics()


def read_params_from_config(config_file):
    with open(config_file, "r") as f:
        for line in f.readlines():
            tokens = line.split("=")
            param = tokens[0].lower()
            value = float(tokens[1].strip("\'\n"))
            exec(f"params.{param} = {value}")


def write_line(out_csv):
    out_csv.write(f"{diag.sampleRTT:.3f}, {diag.estimatedRTT:.3f}, {diag.devRTT:.3f}, {diag.live_quantum:.3f}\n")


def set_initial_values(first_line):
    diag.sampleRTT = float(first_line.split(",")[0])
    diag.estimatedRTT = diag.sampleRTT
    diag.live_quantum = 50


def update_rtt(line):
    diag.sampleRTT = float(line.split(",")[0])
    diag.estimatedRTT = (1 - params.alpha) * diag.estimatedRTT + params.alpha * diag.sampleRTT

    # diag.devRTT = (1 - params.beta) * diag.devRTT + params.beta * abs(diag.estimatedRTT - diag.sampleRTT)
    # diag.devRTT = max((1 - params.beta) * diag.devRTT + params.beta * max(deltas), 10)
    diag.devRTT = (1 - params.beta) * diag.devRTT + params.beta * max(deltas)

#
# def update_quantum():
#     global highRTTCounter
#     global timeoutCounter
#     if diag.sampleRTT != 0 and diag.live_quantum != 0:
#         highRTTCounter = 0
#         est_rtt = diag.estimatedRTT
#         dev_rtt = diag.devRTT
#         delta = diag.sampleRTT - diag.estimatedRTT
#         # if delta < 0:
#         #     highRTTCounter = 0
#         #     if delta < -params.gamma * dev_rtt:
#         #         diag.live_quantum *= params.medium_increase
#         #     elif delta < -0.5 * params.gamma * dev_rtt:
#         #         diag.live_quantum *= params.low_increase
#         # elif delta > 0:
#         #     if highRTTCounter < 3:
#         if delta > 2 * params.tau * dev_rtt:
#             highRTTCounter += 1
#             diag.live_quantum *= params.medium_decrease
#         elif delta >  params.tau * dev_rtt:
#             highRTTCounter += 1
#             diag.live_quantum *= params.low_decrease
#         else:
#             diag.live_quantum += 0.02
#             diag.live_quantum = min(50, diag.live_quantum)
#         # else:
#         #     highRTTCounter = 0
#     # else:
#     #             highRTTCounter = 0
#     #             diag.live_quantum *= params.high_decrease
#     diag.live_quantum = min(2000, diag.live_quantum)

#


# Simple
def update_quantum():
    global highRTTCounter
    global timeoutCounter
    if diag.sampleRTT != 0 and diag.live_quantum != 0:
        est_rtt = diag.estimatedRTT
        dev_rtt = diag.devRTT
        delta = diag.sampleRTT - est_rtt
        if delta < -params.gamma * dev_rtt:
            highRTTCounter = 0
            diag.live_quantum *= params.low_increase
        elif delta > params.tau * dev_rtt:
            highRTTCounter += 1
            if highRTTCounter == HIGH_RTT_MAX:
                diag.live_quantum *= params.low_decrease
                highRTTCounter = 0
        # else:
        #     highRTTCounter = 0
    diag.live_quantum = min(2000, diag.live_quantum)

# """
# Like in application
# """
# def update_quantum():
#     global highRTTCounter
#     global timeoutCounter
#     if diag.sampleRTT != 0 and diag.live_quantum != 0:
#         est_rtt = diag.estimatedRTT
#         dev_rtt = diag.devRTT
#         delta = diag.sampleRTT - est_rtt
#         if delta < -params.gamma * dev_rtt:
#             highRTTCounter = 0
#             if delta < -2 * params.gamma * dev_rtt:
#                 diag.live_quantum *= params.medium_increase
#             else:
#                 diag.live_quantum *= params.low_increase
#         elif delta > params.gamma * dev_rtt:
#             if highRTTCounter < HIGH_RTT_MAX:
#                 highRTTCounter += 1
#             else:
#                 if delta > 2 * params.tau * dev_rtt:
#                     diag.live_quantum *= params.medium_decrease
#                 else:
#                     diag.live_quantum *= params.low_decrease
#                 highRTTCounter = 0
#         else:
#             highRTTCounter = 0
#

    # diag.live_quantum = max(min(2000, diag.live_quantum), 50)
#


def update_deltas():
    deltas.pop(0)
    deltas.append(abs(diag.estimatedRTT - diag.sampleRTT))


def write_to_output_csv(input_csv_file, output_csv):

    with open(input_csv_file, "r") as in_csv:
        with open(output_csv, "w") as out_csv:
            lines = iter(in_csv.readlines())
            first_line = next(lines)
            set_initial_values(first_line)
            write_line(out_csv)
            for i, line in enumerate(lines):
                update_deltas()
                update_rtt(line)
                if i > warmUpIterations:
                    update_quantum()
                write_line(out_csv)


if __name__ == "__main__":
    print("Usage: python3 expected_graph.py <path_to_configuration_file> <path_to_csv>\n")
    configuration_file = sys.argv[1]
    input_csv_file = sys.argv[2]
    read_params_from_config(configuration_file)

    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y")
    timeofday = now.strftime("%H-%M-%S")
    output_path = f"/usr/local/bin/testcases/{dt_string}/{timeofday}"
    os.system(f"mkdir -p {output_path}")
    os.system(f"cp {configuration_file} {output_path}")
    output_csv = output_path + "/graph.csv"
    write_to_output_csv(input_csv_file, output_csv)
    os.system(f"python3 /usr/local/bin/live_plot.py {output_csv}")
    print("success!")
